package com.jowan;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void deposit() {
        MonetaryAmount m = new MonetaryAmount(0, "francs");
        Account a = new Account("Pédro", m);
        a.deposit(500);
        Assertions.assertEquals(500, m.getAmount());
    }

    @Test
    void withdraw() {
        MonetaryAmount m = new MonetaryAmount(2000, "francs");
        Account a = new Account("Pédro", m);
        a.withdraw(500);
        Assertions.assertEquals(1500, m.getAmount());
    }

    @Test
    void tooBigWithdraw() {
        MonetaryAmount m = new MonetaryAmount(35, "francs");
        Account a = new Account("Pédro", m);
        a.withdraw(500);
        Assertions.assertEquals(35, m.getAmount());
    }

    @Test
    void getCurrentBalance() {
        MonetaryAmount m = new MonetaryAmount(0, "francs");
        Account a = new Account("Pédro", m);
        MonetaryAmount gcb = a.getCurrentBalance();
        Assertions.assertEquals(0, gcb.getAmount());
    }

    @Test
    void getOwnerName() {
        MonetaryAmount m = new MonetaryAmount(0, "francs");
        Account a = new Account("Pédro", m);
        Assertions.assertEquals("Pédro", a.getOwnerName());
    }
}