package com.jowan;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MonetaryAmountTest {

    @Test
    void getAmount() {
        MonetaryAmount m = new MonetaryAmount(34, "francs");
        Assertions.assertEquals(34, m.getAmount());
    }

    @Test
    void addAmount() {
        MonetaryAmount m = new MonetaryAmount(34, "francs");
        m.addAmount(8);
        Assertions.assertEquals(42, m.getAmount());

    }

    @Test
    void substractAmount() {
        MonetaryAmount m = new MonetaryAmount(34, "francs");
        m.substractAmount(30);
        Assertions.assertEquals(4, m.getAmount());
    }

    @Test
    void substractAmountforNegResult() {
        MonetaryAmount m = new MonetaryAmount(34, "francs");
        m.substractAmount(50);
        Assertions.assertEquals(-16, m.getAmount());
    }

    @Test
    void testToString() {
        MonetaryAmount m = new MonetaryAmount(34, "francs");
        Assertions.assertEquals("Le montant du MonetaryAmount est de 34.0 francs", m.toString());

    }
}