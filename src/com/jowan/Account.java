package com.jowan;

public class Account {
    private final String ownerName;
    private MonetaryAmount balance;

    public Account(String ownerName, MonetaryAmount balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public MonetaryAmount getCurrentBalance() {
        return balance;
    }

    public void deposit(double amount) {
        balance.addAmount(amount);
    }

    public void withdraw(double amount) {
        if (amount > balance.getAmount()) {
            System.out.println("Le solde actuel de votre compte étant de " +balance.getAmount()+
                    ", il vous est impossible d'effectuer un retrait d'une valeur de " + amount + " " +balance.getCurrency()+ "." );
        } else {
            balance.substractAmount(amount);
        }
    }
}
