package com.jowan;

public class MonetaryAmount {

    protected double amount;
    private final String currency;

    public MonetaryAmount(double amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void addAmount(double a) {
            amount += a;
    }

    public void substractAmount(double a) {
        amount -= a;
    }

    public String toString(){
        return "Le montant du solde de votre compte est de " +getAmount()+ " " +currency+ ".";
    }
}
