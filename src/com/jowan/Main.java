package com.jowan;

public class Main {
    public static void main(String[] args) {

        MonetaryAmount obamount = new MonetaryAmount(0, "sesterces");
        Account obaccount = new Account("Obélix", obamount);

        System.out.println((obaccount.getCurrentBalance()).toString());

        obaccount.deposit(500.5);

        System.out.println(obaccount.getCurrentBalance());

        obaccount.withdraw(125.25);

        System.out.println(obaccount.getCurrentBalance());

        obaccount.withdraw(1337.0);

        System.out.println(obaccount.getCurrentBalance());

    }
}
